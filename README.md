# Scrape_banks Parser

## Getting Started

### Prerequisites

* [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* [python 3](https://docs.python.org/3/using/index.html)
* [latest chromedriver](https://chromedriver.chromium.org/downloads)

### Installing

Go to the directory and install requirements:

```
cd scrape_banks
pip install -r requirements.txt
```

### Configuration

Add `chromedriver` to Windows `%PATH%`. Also, you should update the driver for each new version of Chrome.

Set up these variables in `settings.py` (`User Config` section):

```
DB_ENGINE_URL = 'sqlite:///C:\\projects\\upwork\\Andes Property\\scrape_banks\\db\\transactions.sqlite'

MAIL_USER = "triplesec@gmail.com"
MAIL_PASS = "password"

STATSMAILER_RCPTS = ["contact@andesproperty.com"]

```

If you want to disable email notifications, comment this line in `ITEM_PIPELINES`:

```
'scrape_banks.extensions.CustomStatsMailer': 150,
```

I.e.:

```
EXTENSIONS = {
    # 'scrape_banks.extensions.CustomStatsMailer': 150,
}
```

If you want add/remove accounts, you can always fix `SETTINGS_BANCODECHILE`, `SETTINGS_SCOTIA` and `SETTINGS_XERO` according to your needs.

## Usage

From `scrape_banks` directory just run in command-line shell:

```
scrapy crawl bancochile
scrapy crawl scotiabank
scrapy crawl ximport
```

The first spider updates database records for Bancochile, the second one updates database records for Scotiabank. You can check the project database in whatever viewer your want. For example, in [sqlitebrowser](https://sqlitebrowser.org/).

If you want to schedule scraping and importing, use [Windows Task Scheduler](https://en.wikipedia.org/wiki/Windows_Task_Scheduler). Press `Win+R`, type `taskschd.msc`, press `enter`. Then `create task`, `actions` and `conditions`. In `actions` specify command name (`scrapy`), working directory and command-line arguments (`crawl bancochile`). I suggest you to redirect spider output in log file (i.e. `crawl bancochile > /path/to/bancochile.log`).

More information here:

```
https://www.techsupportalert.com/content/how-schedule-programs-run-automatically-windows-7.htm
https://www.windowscentral.com/how-create-automated-task-using-task-scheduler-windows-10
```
