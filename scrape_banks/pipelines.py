# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html


import logging
import dateparser
import sqlalchemy as sa
from sqlalchemy.sql import text
from scrapy.shell import inspect_response

from scrapy.mail import MailSender
import datetime
import pprint


logging.basicConfig()
logging.getLogger('sqlalchemy.engine')

class DBPipeline(object):

    engine = None
    conn = None
    metadata = None
    rows = []


    @classmethod
    def from_settings(cls, settings):
        engine_url = settings.get('DB_ENGINE_URL', None)
        db_echo = settings.get('DB_ECHO', False)

        return cls(engine_url, db_echo)


    def __init__(self, engine_url, db_echo):
        # warnings.simplefilter("ignore")

        self.engine = sa.create_engine(engine_url, echo=db_echo)
        self.conn = self.engine.connect()

        self.metadata = sa.MetaData(bind=self.engine)
        self.metadata.reflect()

        scrape_banks = sa.Table(   'scrape_banks',
                                    self.metadata,
                                    sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
                                    sa.Column('date', sa.DATE, nullable=False),
                                    sa.Column('bank', sa.String(64), nullable=False),
                                    sa.Column('account', sa.String(64), nullable=False),
                                    sa.Column('description', sa.String(128)),
                                    sa.Column('doc_number', sa.String(128)),
                                    sa.Column('canal', sa.String(128)),
                                    sa.Column('amount', sa.DECIMAL(12,2), nullable=False),
                                    sa.Column('balance', sa.DECIMAL(12,2), nullable=False),
                                    sa.Column("imported", sa.Enum("yes", "no",  "transaction"), server_default="no", nullable=False),
                                    sa.UniqueConstraint('date', 'bank', 'account', 'amount', 'balance', name='uix_1'),
                                    keep_existing=True,
                            )

        scrape_banks.create(checkfirst=True)


    def open_spider(self, spider):
        pass


    def process_item(self, item, spider):
        d = dict(item)
        d['date'] = dateparser.parse(d['date']).date()

        self.rows.append(d)

        return item


    def close_spider(self, spider):
        if self.rows:
            # scrape_banks = sa.Table("scrape_banks", self.metadata, autoload=True)
            # self.conn.execute(scrape_banks.insert(), self.rows)

            upsert = """INSERT INTO scrape_banks (date, bank, account, description, doc_number, canal, amount, balance)
                        VALUES (:date, :bank, :account, :description, :doc_number, :canal, :amount, :balance)
                        ON CONFLICT(date, bank, account, amount, balance) DO UPDATE SET description = EXCLUDED.description;
            """

            self.conn.execute(text(upsert), self.rows)
