# -*- coding: utf-8 -*-
import re
import json
import scrapy
import dateparser
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
from scrapy.exceptions import CloseSpider, DontCloseSpider
from scrapy.shell import inspect_response
from urllib.parse import urlparse, urlunparse, parse_qsl, urlencode, quote


class ScotiabankSpider(scrapy.Spider):
    name = 'scotiabank'
    allowed_domains = ['scotiabank.cl']

    login_url = 'https://www.scotiabank.cl/login/empresas/?nocache=true'
    cabinet_url = '/cgi-bin/transac/dotrx?TMPL=/login/main.html&ori=SSA'
    sent_url = 'https://www.scotiabank.cl/cgi-bin/transac/dotrx?TRANS=vt_CstTef&TMPL=%2Fctacte%2Fretcsttef.html&page=1&fecmin={}&fecmax={}&tipo=A'
    received_url = 'https://www.scotiabank.cl/cgi-bin/transac/dotrx?TMPL=%2Fservicios%2Fcstpgosrecibidos.html&TRANS=vt_CstPagosRecibidos&fecpos=&fecmin={}&fecmax={}&numcor=&exportar=0&rellasig=9999999999999999999999&rutcst=00000000000762822261&fmin={}&fmax={}&tipo=TEF'
    transactions_url = 'https://www.scotiabank.cl/cgi-bin/transac/dotrx?TMPL=/ctacte/cartola.html&TRANS=vt_CartolaCtaCte&cta={}&codtrs=S00400&codprd=01000'

    accounts = []
    sent = []
    received = []

    sent_parsing_is_over = False


    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super().from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_idle, signal=scrapy.signals.spider_idle)
        return spider


    def start_requests(self):
        self.bank = self.settings.get('SETTINGS_SCOTIA')

        if not self.bank:
            raise CloseSpider("Unable to find SETTINGS_SCOTIA")

        self.accounts = self.bank['accounts']

        self.end_date = datetime.now().strftime("%d/%m/%Y")
        self.start_date = (datetime.now() - timedelta(days=self.bank['delta_days'])).date().strftime("%d/%m/%Y")

        yield scrapy.Request(self.login_url, callback=self.do_login, dont_filter=True)


    def do_login(self, response):

        formdata = {
                    'rutCliente': self.bank['cliente'],
                    'rut': self.bank['cliente'],
                    'rutUsuario': self.bank['usario'],
                    'usu': self.bank['usario'],
                    'password': self.bank['clave'],
                    'pin': self.bank['clave'],
                }

        r = scrapy.FormRequest.from_response(response, formdata=formdata, callback=self.check_login)

        return r


    def check_login(self, response):
        # inspect_response(response, self)

        redirect = 'window.location.replace("/cgi-bin/transac/dotrx?TMPL=/login/main.html&ori=SSA")'

        if redirect in response.text:
            return self.after_login(response)
        else:
            return self.login_fail(response)


    def login_fail(self, response):
        # self.logger.critical("Unable to login")
        raise CloseSpider("Something wrong. Unable to login.")


    def after_login(self, response):
        url = self.sent_url.format(quote(self.start_date, safe=''), quote(self.end_date, safe=''))
        return scrapy.Request(url, callback=self.parse_sent)


    def parse_sent(self, response):
        items = response.css('table td.C4I a ::attr(href)').extract()

        for item in items:
            yield response.follow(item, callback=self.parse_sent_item)

        next_url = response.css('div.pagination div.next-page a ::attr(href)').extract_first()
        if next_url:
            yield response.follow(next_url, callback=self.parse_sent)


    def parse_sent_item(self, response):
        # inspect_response(response, self)

        row = _sent(response.text)

        if row:
            self.sent.append(row)


    def spider_idle(self, spider):
        if not spider.sent_parsing_is_over:
            spider.sent_parsing_is_over = True
            url = spider.received_url.format(quote(spider.start_date, safe=''), quote(spider.end_date, safe=''), quote(spider.start_date, safe=''), quote(spider.end_date, safe=''))
            spider.crawler.engine.crawl(scrapy.Request(url, callback=spider.parse_received), spider)
            raise DontCloseSpider('Sent records were handled')


    def parse_received(self, response):
        self.received = _received(response.text)

        for account in self.accounts:
            meta = {'account': account}
            url = self.transactions_url.format(account['account'])
            yield scrapy.Request(url, meta=meta, callback=self.parse_transactions)


    def parse_transactions(self, response):
        # inspect_response(response, self)

        account = response.meta['account']

        scripts = '\n'.join(response.css('script').extract())
        match = re.search("var str = '({.*?})'", scripts)

        if not match:
            raise CloseSpider("Unable to find transactions data")

        json_text = match.group(1)

        try:
            data = json.loads(json_text)
        except ValueError as e:
            raise CloseSpider("Unable to parse json file: {}".format(e))

        #                      0        1           2         3       4        5
        # transaction list: [date, description, doc_number, sent, received, balance]

        rows = [[d['fecmovfmt'], account['account'], d['glosa'], d['numdoc'], d['montomov'] if d['tipomov'] == 'C' else '', d['montomov'] if d['tipomov'] == 'A' else '', d['saldolin']] for d in data['lstCartolaCtaCte']['registros']]

        start_date = dateparser.parse(self.start_date, date_formats=['%d/%m/%Y']).date()

        for row in rows:
            row[2] = row[2].strip()
            row[3] = row[3].lstrip('0')
            row[4] = str(int(int(row[4])/100)) if row[4] else row[4]
            row[5] = str(int(int(row[5])/100)) if row[5] else row[5]
            row[6] = str(int(int(row[6])/100)) if row[6] else row[6]

            row = _match(row, self.sent, self.received)

            d = {}

            d['date'] = dateparser.parse(row[0], date_formats=['%d/%m/%Y']).date()

            if d['date'] < start_date:
                continue

            d['date'] = d['date'].isoformat()

            d['bank'] = self.bank['name']
            d['account'] = account['account']
            d['description'] = row[2]
            d['doc_number'] = row[3]
            d['canal'] = ''

            sent = row[4]
            if sent:
                d['amount'] = '-' + sent

            received = row[5]
            if received:
                d['amount'] = received

            d['balance'] = row[6]

            yield d


########################################################################
# Helpers
########################################################################

def _sent(www):
    out = []

    soup = BeautifulSoup(www, "html.parser")
    # table = soup.findAll('div',{"class":"medium"})[0].findAll('table')[0].findAll('tbody')[0]
    # rows = table.findAll('tr')

    rows = soup.select("div.medium table tr")

    date, amount, name, message = '','','',''
    for row in rows:
        tds = row.findAll('td')

        if not tds:
            continue

        cell1 = tds[0].text.strip()
        cell2 = tds[1].text.strip()
        if cell1 == 'Fecha':
            date = cell2
        elif cell1 == 'Monto Transferido':
            amount = cell2.replace('$','').replace('.','')
        elif cell1 == 'Nombre Destinatario':
            name = cell2
        elif cell1 == 'Mensaje enviado a Destinatario':
            message = cell2
        elif cell1 == 'Desde Cuenta Corriente Nº':
            account = cell2.strip().replace('-', '').lstrip('0').zfill(20)

    if name:
        return [date, account, amount, name, message]
    else:
        return None


def _received(www):
    out = []

    soup = BeautifulSoup(www, "html.parser")
    table = soup.findAll('div',{"class":"large"})[0].findAll('table')[1].find('tbody')
    rows = table.findAll('tr')

    for row in rows:
        columns = row.findAll('td')

        date = columns[0].text.strip()
        types = columns[1].text.strip()
        account = columns[2].text.strip().replace('-', '').lstrip('0').zfill(20)
        amount = columns[3].text.strip().replace('$','').replace('.','')
        bank = columns[4].text.strip()
        from_acc = columns[5].text.strip()
        rut = columns[6].text.strip()
        initiator = columns[7].text.strip()

        out.append([date, types, account, amount, bank, from_acc, rut, initiator])

    return out


def _match(tr, sent=[], received=[]):
    #                      0      1         2           3         4       5        6
    # transaction list: [date, account, description, doc_number, sent, received, balance]
    #               0      1       2      3      4
    # sent list: [date, account, amount, name, message]
    #                   0     1       2        3      4      5       6      7
    # received list: [date, types, account, amount, bank, from_acc, rut, initiator]

    fmt = "%d/%m/%Y"

    if sent:
        if tr[4]:
            for snt in sent:
                if snt:
                    if datetime.strptime(snt[0], fmt).weekday() in [4,5,6]:
                        if (abs(datetime.strptime(tr[0], fmt) - datetime.strptime(snt[0], fmt)) <= timedelta(days=4)) and (tr[4] == snt[2]) and (tr[1] == snt[1]):
                            if ' - ' not in tr[2]:
                                tr[2] = "%s - %s - %s" %(snt[3], snt[4], tr[2])
                                sent.remove(snt)
                    else:
                        if (abs(datetime.strptime(tr[0], fmt) - datetime.strptime(snt[0], fmt)) <= timedelta(days=2)) and (tr[4] == snt[2]) and (tr[1] == snt[1]):
                            if ' - ' not in tr[2]:
                                tr[2] = "%s - %s - %s" %(snt[3], snt[4], tr[2])
                                sent.remove(snt)

    if received:
        if tr[5]:
            for rcv in received:
                if rcv:
                    if (abs(datetime.strptime(tr[0], fmt) - datetime.strptime(rcv[0], fmt)) <= timedelta(days=2)) and (tr[5] == rcv[3]) and (tr[1] == rcv[2]):
                        if ' - ' not in tr[2]:
                            tr[2] = "%s - %s" % (rcv[7], tr[2])
                            received.remove(rcv)

    return tr
