# -*- coding: utf-8 -*-
import os
import csv
import time
import scrapy
import datetime
import sqlalchemy as sa
from sqlalchemy import desc, asc
from scrapy.shell import inspect_response
from scrapy.exceptions import CloseSpider, DontCloseSpider

from ..scrapy_selenium import SeleniumRequest
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class XimportSpider(scrapy.Spider):
    name = 'ximport'
    allowed_domains = ['xero.com']
    login_url = 'https://login.xero.com/'
    import_url = 'https://go.xero.com/Bank/Import.aspx?accountID={}'

    custom_settings = {
        'DOWNLOADER_MIDDLEWARES': {
            'scrape_banks.scrapy_selenium.SeleniumMiddleware': 800
        },
        'ITEM_PIPELINES': {},
    }


    def start_requests(self):
        self.date = datetime.datetime.now()
        self.xero = self.settings.get('SETTINGS_XERO')

        if not self.xero:
            raise CloseSpider("Unable to find SETTINGS_XERO")

        db_engine_url = self.settings.get('DB_ENGINE_URL')
        db_echo = self.settings.get('DB_ECHO')

        db_engine_url = self.settings.get('DB_ENGINE_URL')
        db_echo = self.settings.get('DB_ECHO')

        self.engine = sa.create_engine(db_engine_url, echo=db_echo)
        self.conn = self.engine.connect()
        self.metadata = sa.MetaData(bind=self.engine)
        self.metadata.reflect()

        yield SeleniumRequest(url=self.login_url, callback=self.do_loging, wait_time=15, wait_until=EC.element_to_be_clickable((By.CSS_SELECTOR, 'button#submitButton')))


    def do_loging(self, response):
        driver = response.meta['driver']

        driver.find_element_by_css_selector('input#email').clear()
        driver.find_element_by_css_selector('input#email').send_keys(self.xero['login'])
        time.sleep(1)

        driver.find_element_by_css_selector('input#password').clear()
        driver.find_element_by_css_selector('input#password').send_keys(self.xero['password'])
        time.sleep(1)

        driver.find_element_by_css_selector('button#submitButton').click()

        try:
            WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'a[data-navigation-id="logout"]')))
            if self.xero['banks']:
                bank = self.xero['banks'].pop(0)
                url = self.import_url.format(bank['xero_id'])
                meta = {'bank': bank}
                yield SeleniumRequest(url=url, meta=meta, callback=self.statements_import, wait_time=15, wait_until=EC.element_to_be_clickable((By.CSS_SELECTOR, 'button[onclick="SubmitAction(\'UploadFile\');"]')))
        except Exception as e:
            raise CloseSpider("Unable to login: {}".format(e))


    def statements_import(self, response):
        # inspect_response(response, self)

        driver = response.meta['driver']
        bank = response.meta['bank']

        if bank['transactions_retry']:
            imported_status = ['no', 'transaction']
        else:
            imported_status = ['no']

        scrape_banks = sa.Table("scrape_banks", self.metadata, autoload=True)
        s = sa.select([scrape_banks.c.id, scrape_banks.c.date, scrape_banks.c.amount, scrape_banks.c.description]).\
                        where(scrape_banks.c.bank == bank['db_name']).\
                        where(scrape_banks.c.account == bank['account']).\
                        where(scrape_banks.c.imported.in_(imported_status)).\
                        order_by(asc(scrape_banks.c.date))

        rows = list(self.conn.execute(s))

        if rows:
            path = _prepare_csv(self, bank, rows)
            if path:
                ids = [t[0] for t in rows]
                s = sa.update(scrape_banks).\
                        where(scrape_banks.c.bank == bank['db_name']).\
                        where(scrape_banks.c.account == bank['account']).\
                        where(scrape_banks.c.id.in_(ids)).values(imported='transaction')
                self.conn.execute(s)

                driver.find_element_by_css_selector('input#UploadFile').send_keys(path)
                time.sleep(1)

                driver.find_element_by_css_selector('button[onclick="SubmitAction(\'UploadFile\');"]').click()
                time.sleep(10)

                try:
                    driver.find_element_by_css_selector('a#saveButton').click()
                    time.sleep(10)
                except:
                    pass

                # inspect_response(response, self)

                response = response.replace(body=driver.page_source)
                (is_error, bad_ids) = _check_errors(self, response)

                if is_error:
                    if bad_ids:
                        s = sa.update(scrape_banks).\
                                        where(scrape_banks.c.bank == bank['db_name']).\
                                        where(scrape_banks.c.account == bank['account']).\
                                        where(scrape_banks.c.id.in_(bad_ids)).values(imported='no')
                        self.conn.execute(s)
                else:
                    s = sa.update(scrape_banks).\
                            where(scrape_banks.c.bank == bank['db_name']).\
                            where(scrape_banks.c.account == bank['account']).\
                            where(scrape_banks.c.id.in_(ids)).values(imported='yes')
                    self.conn.execute(s)

                if bank['make_screenshot']:
                    driver.get_screenshot_as_file(path.replace('.csv', '.png'))
        else:
            self.logger.info('The new transactions have not been found for {}'.format(bank['db_name']))

        if self.xero['banks']:
            bank = self.xero['banks'].pop(0)
            url = self.import_url.format(bank['xero_id'])
            meta = {'bank': bank}
            yield SeleniumRequest(url=url, meta=meta, callback=self.statements_import, wait_time=15, wait_until=EC.element_to_be_clickable((By.CSS_SELECTOR, 'button[onclick="SubmitAction(\'UploadFile\');"]')))


########################################################################
# Helpers
########################################################################

def _prepare_csv(spider, bank, rows):
    fmt = "%Y-%m-%d %H-%M-%S"
    filename = '{} {}.csv'.format(bank['db_name'], spider.date.strftime(fmt))
    path = os.path.join(spider.xero['import_dir'], filename)

    header = ['Date', 'Amount', 'Description']
    with open(path, 'w', encoding='utf-8', newline='') as fout:
        writer = csv.writer(fout, delimiter=',', quotechar='"')
        writer.writerow(header)

        for row in rows:
            (_id, date, amount, description) = row
            r = (date, amount, description)
            writer.writerow(r)

    return path


def _check_errors(spider, response):
    is_error = False
    bad_ids = []

    return is_error, bad_ids