# -*- coding: utf-8 -*-
import re
import time
import scrapy
import dateparser
from scrapy.shell import inspect_response
from scrapy.exceptions import CloseSpider, DontCloseSpider

from ..scrapy_selenium import SeleniumRequest
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException


class XexportSpider(scrapy.Spider):
    name = 'xexport'
    allowed_domains = ['xero.com']
    login_url = 'https://login.xero.com/'
    export_urls = [
        'https://go.xero.com/Bank/Statements.aspx?accountID=73D19F21-7CAE-4E50-9333-615B2275351E',
        'https://go.xero.com/Bank/Statements.aspx?accountID=1996AE14-7705-4A1D-8417-62C79CEAB5D0'
    ]

    custom_settings = {
        'DOWNLOADER_MIDDLEWARES': {
            'scrape_banks.scrapy_selenium.SeleniumMiddleware': 800
        },
        'ITEM_PIPELINES': {},
        'EXTENSIONS': {},
    }


    def start_requests(self):
        self.xero = self.settings.get('SETTINGS_XERO')

        if not self.xero:
            raise CloseSpider("Unable to find SETTINGS_XERO")


        yield SeleniumRequest(url=self.login_url, callback=self.do_loging, wait_time=15, wait_until=EC.element_to_be_clickable((By.CSS_SELECTOR, 'button#submitButton')))


    def do_loging(self, response):
        driver = response.meta['driver']

        driver.find_element_by_css_selector('input#email').clear()
        driver.find_element_by_css_selector('input#email').send_keys(self.xero['login'])
        time.sleep(1)

        driver.find_element_by_css_selector('input#password').clear()
        driver.find_element_by_css_selector('input#password').send_keys(self.xero['password'])
        time.sleep(1)

        driver.find_element_by_css_selector('button#submitButton').click()

        try:
            WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'a[data-navigation-id="logout"]')))
            driver.get_screenshot_as_file('ok.png')
            if self.export_urls:
                url = self.export_urls.pop(0)
                yield SeleniumRequest(url=url, callback=self.parse_statements, wait_time=15, wait_until=EC.element_to_be_clickable((By.CSS_SELECTOR, 'select#mainPagerItemPerPageDropDown')))
        except:
            # driver.get_screenshot_as_file('error.png')
            raise CloseSpider("Unable to login")


    def parse_statements(self, response):
        # inspect_response(response, self)

        driver = response.meta['driver']

        while True:
            yield from _parse_statements(response)
            try:
                next_page_css = 'li#pagerNext:not([style="display: none;"]) a'
                next_page = driver.find_element_by_css_selector(next_page_css)
                next_page.click()
                time.sleep(5)
                response = response.replace(body=driver.page_source)
            except NoSuchElementException:
                break

        if self.export_urls:
            url = self.export_urls.pop(0)
            yield SeleniumRequest(url=url, callback=self.parse_statements, wait_time=15, wait_until=EC.element_to_be_clickable((By.CSS_SELECTOR, 'select#mainPagerItemPerPageDropDown')))



########################################################################
# Helpers
########################################################################

def _parse_statements(response):
    tr_css = 'table#statementDetails tr.slg-row.bank-transactions-table-row'
    trs = response.css(tr_css)

    bank_xpath = '//*[@id="ext-gen41"]/text()'
    bank = response.xpath(bank_xpath).extract_first()

    for tr in trs:
        tds = [' '.join(td.css('::text').extract()) for td in tr.css('td')]
        [select, date, statement_type, payee, description, code, reference, analysis_code, spent, received, balance, source, status] = tds

        d = {}

        d['bank'] = bank
        d['date'] = dateparser.parse(date).date().isoformat()
        d['statement_type'] = statement_type
        d['payee'] = payee
        d['description'] = description
        d['code'] = code
        d['reference'] = reference
        d['analysis_code'] = analysis_code
        if spent:
            d['amount'] = '-' + re.sub('[ ,]', '', spent)
        elif received:
            d['amount'] = re.sub('[ ,]', '', received)
        else:
            d['amount'] = None
        d['balance'] = re.sub('[ ,)(]', '', balance)
        d['source'] = source
        d['status'] = status

        yield d
