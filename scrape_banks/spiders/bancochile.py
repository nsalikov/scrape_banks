# -*- coding: utf-8 -*-
import re
import json
import scrapy
import dateparser
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
from scrapy.exceptions import CloseSpider, DontCloseSpider
from scrapy.shell import inspect_response
from urllib.parse import urlparse, urlunparse, parse_qsl, urlencode, quote


class BancochileSpider(scrapy.Spider):
    name = 'bancochile'
    allowed_domains = ['bancochile.cl']

    login_url = 'https://www.empresas.bancochile.cl/cgi-bin/navega?pagina=enlinea/login_fus'
    cabinet_url = 'https://www.empresas.bancochile.cl/cgi-bin/navega?pagina=privado/index'
    sent_url = 'https://www.empresas.bancochile.cl/TefMasivasWEB/consulta.do?accion=consulta&llavePre=&llaveIns=&rutIns=&pag=&campo=&initDate={}&endDate={}&cuentaCargo={}&destinatario=&operacion=&estado=&nada=nada'
    received_url = 'https://www.empresas.bancochile.cl/GlosaInternetEmpresaRecibida/RespuestaConsultaRecibidaAction.do?accion=buscarOperaciones&initDate={}&endDate={}&ctaCorriente={}&nada=nada'
    transactions_html_url = 'https://www.empresas.bancochile.cl/CCOLSaldoMovimientosWEB/selectorCuentas.do?accion=initSelectorCuentas&cuenta={}&moneda={}'
    transactions_json_url = 'https://www.empresas.bancochile.cl/CCOLSaldoMovimientosWEB/generadorMovimientosJSON.do?sEcho=2&iColumns=7&sColumns=&iDisplayStart=0&iDisplayLength=100&sNames=%2C%2C%2C%2C%2C%2C&iSortingCols=1&iSortCol_0=0&sSortDir_0=desc&bSortable_0=true&bSortable_1=false&bSortable_2=false&bSortable_3=false&bSortable_4=false&bSortable_5=false&bSortable_6=false'

    accounts = []
    sent = []
    received = []


    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super().from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_idle, signal=scrapy.signals.spider_idle)
        return spider


    def start_requests(self):
        self.bank = self.settings.get('SETTINGS_BANCODECHILE')

        if not self.bank:
            raise CloseSpider("Unable to find SETTINGS_BANCODECHILE")

        self.accounts = self.bank['accounts']

        self.end_date = datetime.now().strftime("%d/%m/%Y")
        self.start_date = (datetime.now() - timedelta(days=self.bank['delta_days'])).date().strftime("%d/%m/%Y")

        yield scrapy.Request(self.login_url, callback=self.do_login, dont_filter=True)


    def do_login(self, response):

        formdata = {
                    'rut_emp': self.bank['empresa'],
                    'dv_emp': self.bank['empresa2'],
                    'rut_apo': self.bank['usario'],
                    'dv_apo': self.bank['usario2'],
                    'pin': self.bank['clave'],
                }

        r = scrapy.FormRequest.from_response(response, formdata=formdata, callback=self.check_login)

        return r


    def check_login(self, response):
        # inspect_response(response, self)

        onload = response.css('body::attr(onload)').extract_first()

        if not onload:
            raise CloseSpider("Unable to login.")

        if self.cabinet_url in onload:
            return self.after_login(response)
        else:
            return self.login_fail(response)


    def login_fail(self, response):
        # self.logger.critical("Unable to login")
        raise CloseSpider("Something wrong. Unable to login.")


    def after_login(self, response):
        if not self.accounts:
            return

        self.sent = []
        self.received = []

        account = self.accounts.pop(0)
        meta = {'account': account}

        url = self.sent_url.format(quote(self.start_date, safe=''), quote(self.end_date, safe=''), account['account'])
        return scrapy.Request(url, meta=meta, callback=self.parse_sent)


    def parse_sent(self, response):
        # inspect_response(response, self)

        account = response.meta['account']
        meta = {'account': account}

        self.sent = _parse_sent(response.text)

        url = self.received_url.format(quote(self.start_date, safe=''), quote(self.end_date, safe=''), account['account'])
        return scrapy.Request(url, meta=meta, callback=self.parse_received)


    def parse_received(self, response):
        # inspect_response(response, self)

        account = response.meta['account']
        meta = {'account': account}

        self.received = _parse_received(response.text)

        url = self.transactions_html_url.format(account['account'], account['currency'])
        return scrapy.Request(url, meta=meta, callback=self.parse_transactions_html)


    def parse_transactions_html(self, response):
        account = response.meta['account']
        meta = {'account': account}

        return scrapy.Request(self.transactions_json_url, meta=meta, callback=self.parse_transactions_json, dont_filter=True)


    def parse_transactions_json(self, response):
        account = response.meta['account']
        meta = {'account': account}

        start_date = dateparser.parse(self.start_date, date_formats=['%d/%m/%Y']).date()

        try:
            data = json.loads(response.text)
            for row in data['aaData']:
                row[4] = _fix_money(_get_text(row[4]))
                row[5] = _fix_money(_get_text(row[5]))
                row[6] = _fix_money(_get_text(row[6]))

                row = _match(row, self.sent, self.received)

                d = {}

                d['date'] = dateparser.parse(row[0], date_formats=['%d/%m/%Y']).date()

                if d['date'] < start_date:
                    continue

                d['date'] = d['date'].isoformat()

                d['bank'] = self.bank['name']
                d['account'] = account['account']
                d['description'] = row[1]
                d['doc_number'] = re.sub('[^0-9]', '', row[2])
                d['canal'] = row[3]

                sent = row[4]
                if sent:
                    d['amount'] = '-' + sent.strip()

                received = row[5]
                if received:
                    d['amount'] = received.strip()

                d['balance'] = row[6]

                yield d

        except ValueError as e:
            raise CloseSpider("Unable to parse json file: {}".format(e))


    def spider_idle(self, spider):
        if spider.accounts:
            spider.crawler.engine.crawl(scrapy.Request(spider.cabinet_url, callback=spider.after_login, dont_filter=True), spider)
            raise DontCloseSpider('Parsing new account')

########################################################################
# Helpers
########################################################################


def _get_text(html):
    soup = BeautifulSoup(html, 'lxml')
    text = soup.get_text()

    return text


def _match(tr, sent=[], received=[]):
    #                0         1           2       3      4       5        6
    # transaction: date, description, doc_number, canal, sent, received, balance
    #               0     1       2       3            4          5        6       7
    # sent list: [date, position, name, operation, account_num, comment, amount, status]
    #                   0     1        2      3     4    5      6       7
    # received list: [date, source, deposit, name, rut, bank, amount, status]

    fmt = "%d/%m/%Y"

    if sent:
        if tr[4]:
            for snt in sent:
                if snt:
                    if datetime.strptime(snt[0], "%d/%m/%Y %H:%M").weekday() in [4,5,6]:
                        if (abs(datetime.strptime(tr[0], fmt) - datetime.strptime(snt[0], "%d/%m/%Y %H:%M")) <= timedelta(days=4)) and (tr[4] == snt[6]) and (snt[7] == "Aceptada"):
                            if ' - ' not in tr[1]:
                                tr[1] = "%s - %s - %s" % (snt[5], snt[2], tr[1])
                    else:
                        if (abs(datetime.strptime(tr[0], fmt) - datetime.strptime(snt[0], "%d/%m/%Y %H:%M")) <= timedelta(days=3)) and (tr[4] == snt[6]) and (snt[7] == "Aceptada"):
                            if ' - ' not in tr[1]:
                                tr[1] = "%s - %s - %s" % (snt[5], snt[2], tr[1])

    if received:
        if tr[5]:
            for rcv in received:
                if rcv:
                    if (abs(datetime.strptime(tr[0], fmt) - datetime.strptime(rcv[0], fmt)) <= timedelta(days=2)) and (tr[5] == rcv[6].replace(' ','')) and (rcv[7] == "Aprobada"):
                        if '-' not in tr[1]:
                            tr[1] = "%s - %s" % (rcv[3], tr[1])

    return tr


def _parse_sent(www):
    soup = BeautifulSoup(www, "html.parser")
    out = []

    table = soup.findAll('table',{"id":"tabla_simple"})

    if not table:
        return out
    else:
        table = table[0]

    rows = table.findAll('tr')[3:]
    for row in rows:
        columns = row.findAll('td')

        date = columns[0].text.strip()
        position = columns[1].text.strip()
        name = columns[2].text.strip()
        operation = columns[3].text.strip()
        account_num = columns[4].text.strip()
        comment = columns[5].text.strip()
        amount = _fix_money(columns[6].text.strip())
        status = columns[7].text.strip()

        out.append([date, position, name, operation, account_num, comment, amount, status])

    return out


def _parse_received(www):
    soup = BeautifulSoup(www, "html.parser")
    out = []

    table = soup.findAll('table',{"id":"tablaLista"})

    if not table:
        return out
    else:
        table = table[0].find('tbody')

    rows = table.findAll('tr')
    for row in rows:
        columns = row.findAll('td')

        date = columns[0].text.strip()
        source = columns[1].text.strip()
        deposit = columns[2].text.strip()
        name = columns[3].text.strip()
        rut = columns[4].text.strip()
        bank = columns[5].text.strip()
        amount = _fix_money(columns[6].text.strip())
        status = columns[7].text.strip()

        out.append([date, source, deposit, name, rut, bank, amount, status])

    return out


def _fix_money(money):
    money = money.replace('.','').replace(' ','').replace(',', '.')

    return money