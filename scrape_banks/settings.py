# -*- coding: utf-8 -*-

# Scrapy settings for scrape_banks project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://doc.scrapy.org/en/latest/topics/settings.html
#     https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://doc.scrapy.org/en/latest/topics/spider-middleware.html

import os
from shutil import which


CWD = os.path.dirname(os.path.realpath(__file__))

BOT_NAME = 'scrape_banks'

SPIDER_MODULES = ['scrape_banks.spiders']
NEWSPIDER_MODULE = 'scrape_banks.spiders'

# Obey robots.txt rules
ROBOTSTXT_OBEY = False

# Configure maximum concurrent requests performed by Scrapy (default: 16)
#CONCURRENT_REQUESTS = 32

# Configure a delay for requests for the same website (default: 0)
# See https://doc.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
DOWNLOAD_DELAY = 1
# The download delay setting will honor only one of:
#CONCURRENT_REQUESTS_PER_DOMAIN = 16
#CONCURRENT_REQUESTS_PER_IP = 16

# Disable cookies (enabled by default)
#COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED = False

# Override the default request headers:
DEFAULT_REQUEST_HEADERS = {
  'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
  'Accept-Language': 'en-US,en;q=0.5',
  'Accept-Encoding': 'gzip',
}

# Enable or disable spider middlewares
# See https://doc.scrapy.org/en/latest/topics/spider-middleware.html
#SPIDER_MIDDLEWARES = {
#    'scrape_banks.middlewares.ScrapeBanksSpiderMiddleware': 543,
#}

# Enable or disable downloader middlewares
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#DOWNLOADER_MIDDLEWARES = {
#    'scrape_banks.middlewares.ScrapeBanksDownloaderMiddleware': 543,
#}

# Configure item pipelines
# See https://doc.scrapy.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
   'scrape_banks.pipelines.DBPipeline': 100,
}

# Enable or disable extensions
# See https://doc.scrapy.org/en/latest/topics/extensions.html
EXTENSIONS = {
    # 'scrape_banks.extensions.CustomStatsMailer': 150,
}

# Enable and configure the AutoThrottle extension (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/autothrottle.html
#AUTOTHROTTLE_ENABLED = True
# The initial download delay
#AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPCACHE_ENABLED = True
#HTTPCACHE_EXPIRATION_SECS = 0
#HTTPCACHE_DIR = 'httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES = []
#HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'

#########################################################
# User Config
#########################################################

LOG_LEVEL = 'DEBUG'

FEED_EXPORT_ENCODING = 'utf-8'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36"

# mysql://scott:tiger@localhost/test
# postgresql://scott:tiger@localhost/mydatabase
# postgresql+psycopg2://scott:tiger@localhost/mydatabase
# mysql://scott:tiger@localhost/foo
# mysql+mysqldb://scott:tiger@localhost/foo
# mysql+mysqlconnector://scott:tiger@localhost/foo
# sqlite:////absolute/path/to/foo.db
# sqlite:///so-dump.db

# see more: https://docs.sqlalchemy.org/en/13/core/connections.html

DB_ENGINE_URL = 'sqlite:///C:\\projects\\upwork\\Andes Property\\scrape_banks\\db\\transactions.sqlite'
DB_ECHO = True

SETTINGS_BANCODECHILE = {
    # removed
    "delta_days": 30, # no more 45
    "accounts": [
        {"account": "000019685604", "currency": "CTD"},
        {"account": "050019685609", "currency": "CEX"},
    ]
}

SETTINGS_SCOTIA = {
    # removed
    "delta_days": 30,
    "accounts": [
        {"account": "00000000000972107514"},
        {"account": "00000000000975328376"},
        {"account": "00000000090972236209"},
    ]
}

SETTINGS_XERO = {
    # removed
    "import_dir": "C:\\projects\\upwork\\Andes Property\\scrape_banks\\import",
    "banks": [
        {
            "db_name": "Banco de Chile CLP",
            "account": "000019685604",
            "xero_id": "73D19F21-7CAE-4E50-9333-615B2275351E",
            "transactions_retry": True,
            "make_screenshot": True,
        },
        # {
        #     "db_name": "Banco de Chile CLP",
        #     "account": "050019685609",
        #     "xero_id": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
        #     "transactions_retry": True,
        #     "make_screenshot": True,
        # },
        {
            "db_name": "Scotiabank CLP",
            "account": "00000000000972107514",
            "xero_id": "1996AE14-7705-4A1D-8417-62C79CEAB5D0",
            "transactions_retry": True,
            "make_screenshot": True,
        },
        # {
        #     "db_name": "Scotiabank CLP",
        #     "account": "00000000000975328376",
        #     "xero_id": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
        #     "transactions_retry": True,
        #     "make_screenshot": True,
        # },
        # {
        #     "db_name": "Scotiabank CLP",
        #     "account": "00000000090972236209",
        #     "xero_id": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
        #     "transactions_retry": True,
        #     "make_screenshot": True,
        # },
    ],
}

# Mail settings for email notifications.
# Current settings are for gmail smtp server.
# Make sure to allow less secure apps on Gmail via this link: https://myaccount.google.com/lesssecureapps

MAIL_HOST = "smtp.gmail.com"
MAIL_PORT = 587
MAIL_USER = "login"
MAIL_PASS = "password"

STATSMAILER_RCPTS = ["removed"]

SELENIUM_DRIVER_NAME = 'chrome'
SELENIUM_DRIVER_EXECUTABLE_PATH = which('chromedriver')
SELENIUM_DRIVER_ARGUMENTS = ['--headless', '--window-size=1024,768', '--log-level=1']
