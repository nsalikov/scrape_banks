import pprint
import datetime
import collections
from scrapy.extensions.statsmailer import StatsMailer

class CustomStatsMailer(StatsMailer):

    def spider_closed(self, spider):
        global_stats = self.stats.get_stats().items()
        spider_stats = self.update_stats(self.stats.get_stats(spider))
        # spider_stats = spider.crawler.stats.get_stats()

        dt = datetime.datetime.now().strftime("%Y/%m/%d %H:%M")
        subject = '[scrape_banks] Report for {} on {}'.format(spider.name, dt)
        intro = "Summary stats from {} spider: \n\n".format(spider.name)
        body = '\n'.join(["{}: {}".format(k, v) for k,v in spider_stats.items()])
        body = intro + body

        return self.mail.send(self.recipients, subject=subject, body=body, mimetype = 'text/plain', charset = 'utf-8')


    def update_stats(self, stats):
        fields = ['start_time', 'finish_time', 'finish_reason', 'item_scraped_count']

        od = collections.OrderedDict()

        for key in fields:
            if key in stats:
                od[key] = stats[key]

        if 'start_time' in od:
            od['start_time'] = od['start_time'].strftime("%Y/%m/%d %H:%M")

        if 'finish_time' in od:
            od['finish_time'] = od['finish_time'].strftime("%Y/%m/%d %H:%M")

        status =  [k for k in stats.keys() if 'response_status_count' in k]

        for s in status:
            od[s] = stats[s]

        msg =  [m for m in stats.keys() if 'msg' in m]

        for m in msg:
            od[m] = stats[m]

        return od
